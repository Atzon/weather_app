/****** Object:  Database [Weather]    Script Date: 31.01.2018 21:50:05 ******/
CREATE DATABASE [Weather]
 CONTAINMENT = NONE

GO
USE [Weather]
GO
/****** Object:  Table [dbo].[Type]    Script Date: 31.01.2018 21:50:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Type](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_Type] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Weather]    Script Date: 31.01.2018 21:50:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Weather](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [date] NULL,
	[Degree] [decimal](2, 0) NULL,
	[TypeID] [int] NULL,
 CONSTRAINT [PK_Weather] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Weather]  WITH CHECK ADD  CONSTRAINT [FK_Weather_Type] FOREIGN KEY([TypeID])
REFERENCES [dbo].[Type] ([ID])
GO
ALTER TABLE [dbo].[Weather] CHECK CONSTRAINT [FK_Weather_Type]
GO
USE [master]
GO
ALTER DATABASE [Weather] SET  READ_WRITE 
GO

Use Weather
GO
INSERT INTO Type (Name) VALUES('Slonecznie')
GO
INSERT INTO Type (Name) VALUES('Deszczowo')
GO
INSERT INTO Type (Name) VALUES('Czesciowe zachmurzenie')
GO

USE Weather
GO 
INSERT INTO Weather (Date, Degree, TypeID) VALUES (GETDATE()-2, -7, (SELECT ID from Type where Name like 'Slonecznie'))
GO 
INSERT INTO Weather (Date, Degree, TypeID) VALUES (GETDATE()-1, -2, (SELECT ID from Type where Name like 'Deszczowo'))
GO 
INSERT INTO Weather (Date, Degree, TypeID) VALUES (GETDATE(),    0, (SELECT ID from Type where Name like 'Slonecznie')) 
GO 
INSERT INTO Weather (Date, Degree, TypeID) VALUES (GETDATE()+1,  1, (SELECT ID from Type where Name like 'Slonecznie'))
GO 
INSERT INTO Weather (Date, Degree, TypeID) VALUES (GETDATE()+2,  2, (SELECT ID from Type where Name like 'Czesciowe zachmurzenie'))
GO 
INSERT INTO Weather (Date, Degree, TypeID) VALUES (GETDATE()+3, -3, (SELECT ID from Type where Name like 'Slonecznie'))
GO 
INSERT INTO Weather (Date, Degree, TypeID) VALUES (GETDATE()+4, -4, (SELECT ID from Type where Name like 'Deszczowo'))
GO 
