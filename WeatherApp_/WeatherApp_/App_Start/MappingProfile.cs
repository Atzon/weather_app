﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeatherApp_.Dtos;


namespace WeatherApp_.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<Models.Weather, WeatherDto>();
            Mapper.CreateMap<WeatherDto, Models.Weather>();
            Mapper.CreateMap<Models.Type, TypeDto>();
            Mapper.CreateMap<TypeDto, Models.Type>();
        }
    }
}