﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WeatherApp_.Dtos
{
    public class TypeDto
    {
        public int ID { get; set; }

        public string Name { get; set; }
    }
}