﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherApp_.Dtos
{
    public class WeatherDto
    {
        public int ID { get; set; }

        public DateTime? Date { get; set; }

        public decimal? Degree { get; set; }

        public TypeDto Type { get; set; }

        public int? TypeID { get; set; }

    }
}