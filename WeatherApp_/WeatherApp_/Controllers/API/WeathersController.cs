﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WeatherApp_.Dtos;
using WeatherApp_.Models;
using System.Data.Entity; 

namespace WeatherApp_.Controllers.API
{
    public class WeathersController : ApiController
    {
        private WeatherContextDb _context;

        public WeathersController()
        {
            _context = new WeatherContextDb();
        }

        // GET /api/weathers
        public IHttpActionResult GetWeathers()
        {
            return Ok(_context.Weather.Include(x => x.Type)
                                      .ToList()
                                      .Select(Mapper.Map<Weather, WeatherDto>));
        }

        // GET /api/weathers/1 
        public IHttpActionResult GetWeather(int id)
        {
            var weather = _context.Weather.SingleOrDefault(x => x.ID == id);

            if (weather == null)
                return NotFound();

            return Ok(Mapper.Map<Weather, WeatherDto>(weather));
        }

        // POST /api/weathers
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public IHttpActionResult CreateWeather(WeatherDto weatherDto)
        {
            if (!ModelState.IsValid)
                return BadRequest(); 

            var weather = Mapper.Map<WeatherDto, Weather>(weatherDto);

            _context.Weather.Add(weather);
            _context.SaveChanges();

            weatherDto.ID = weather.ID;

            return Created(new Uri(Request.RequestUri + "/" + weather.ID), weatherDto);
        }

        // PUT /api/weathers/1 
        [HttpPut]
        [Authorize(Roles = "Administrator")]
        public IHttpActionResult UpdateWeather(int id, WeatherDto weatherDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var weatherInDb = _context.Weather.SingleOrDefault(x => x.ID == id);

            if (weatherInDb == null)
                return NotFound();

          
            Mapper.Map(weatherDto, weatherInDb);

            weatherInDb.Type = _context.Type.SingleOrDefault(x => x.ID == weatherDto.TypeID);

            _context.SaveChanges();

            return Ok(weatherDto);

        
           
        }

        // DELETE /api/weathers/1 
        [HttpDelete]
        [Authorize(Roles = "Administrator")]
        public IHttpActionResult DeleteWeather(int id)
        {
            var weatherInDb = _context.Weather.SingleOrDefault(x => x.ID == id);

            if (weatherInDb == null)
                return NotFound();

            var weatherDto = Mapper.Map<Weather, WeatherDto>(weatherInDb);

            _context.Weather.Remove(weatherInDb);
            _context.SaveChanges();

           
            return Ok(); 

        }

    }
}
