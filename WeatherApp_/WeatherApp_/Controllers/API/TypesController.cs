﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WeatherApp_.Dtos;
using WeatherApp_.Models;

namespace WeatherApp_.Controllers.API
{
    public class TypesController : ApiController
    {
        private WeatherContextDb _context;

        public TypesController()
        {
            _context = new WeatherContextDb();
        }

        // GET /api/types
        public IHttpActionResult GetTypes()
        {
            return Ok(_context.Type.ToList().Select(Mapper.Map<Models.Type, TypeDto>));
        }

        // GET /api/types/1 
        public IHttpActionResult GetType(int id)
        {
            var type = _context.Type.SingleOrDefault(x => x.ID == id);

            if (type == null)
                return NotFound();

            return Ok(Mapper.Map<Models.Type, TypeDto>(type));
        }

        // POST /api/types
        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public IHttpActionResult CreateType(TypeDto typeDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var type = Mapper.Map<TypeDto, Models.Type>(typeDto);

            _context.Type.Add(type);
            _context.SaveChanges();

            typeDto.ID = type.ID;

            return Created(new Uri(Request.RequestUri + "/" + type.ID), typeDto);
        }

        // PUT /api/types/1 
        [HttpPut]
        [Authorize(Roles = "Administrator")]
        public IHttpActionResult UpdateType(int id, TypeDto typeDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var typeInDb = _context.Type.SingleOrDefault(x => x.ID == id);

            if (typeInDb == null)
                return NotFound();


            _context.SaveChanges();

            typeDto.ID = typeInDb.ID;

            return Ok();
        }

        // DELETE /api/types/1 
        [HttpDelete]
        [Authorize(Roles = "Administrator")]
        public IHttpActionResult DeleteType(int id)
        {
            var typeInDb = _context.Type.SingleOrDefault(x => x.ID == id);

            if (typeInDb == null)
                return NotFound();

            var typeDto = Mapper.Map<Models.Type, TypeDto>(typeInDb);

            _context.Type.Remove(typeInDb);
            _context.SaveChanges();


            return Ok();

        }
    }
}
