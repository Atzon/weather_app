﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WeatherApp_.Controllers.API;
using WeatherApp_.Models;

namespace WeatherApp_.Controllers
{
    // global filter [Authorize]
    public class HomeController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult About()
        {        
            return View();
        }
        // global filter [Authorize]
        public ActionResult Days()
        {
            return View();
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Edit(int id)
        {
            ViewBag.id = id; 
            return View(); 
        }
    }
}